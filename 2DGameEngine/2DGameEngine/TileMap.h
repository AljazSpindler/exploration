#pragma once
#include <string>

class TileMap
{
public:
	TileMap(std::string tId, int mS, int tS);
	~TileMap();

	void LoadMap(std::string path, int sizeX, int sizeY);
	void AddTile(int sourceX, int sourceY, int xPos, int yPos);

private:
	std::string texId;
	int mapScale;
	int tileSize;
	int scaledSize;
};

