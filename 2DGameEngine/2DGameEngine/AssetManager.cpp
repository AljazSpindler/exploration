#include "AssetManager.h"
#include "Components.h"

AssetManager::AssetManager()
{
}

AssetManager::AssetManager(Manager* man)
{
	manag = man;
}

AssetManager::~AssetManager()
{
}

void AssetManager::CreateProjectile(Vector2D pos, Vector2D vel, int range, int speed, std::string id)
{
	auto& projectile(manag->addEntity());
	projectile.addComponent<TransformComponent>(pos.x, pos.x, 32, 32, 1);
	projectile.addComponent<SpriteComponent>(id, false);
	projectile.addComponent<ProjectileComponent>(range, speed, vel);
	projectile.addComponent<ColliderComponent>("projectile");
	projectile.addGroup(Game::groupProjectiles);
}

void AssetManager::AddTexture(std::string id, const char * path)
{
	textures.emplace(id, TextureManager::LoadTexture(path));
}

SDL_Texture* AssetManager::GetTexture(std::string id)
{
	return textures[id];
}

void AssetManager::AddFont(std::string id, const char * path, int fontSize)
{
	fonts.emplace(id, TTF_OpenFont(path, fontSize));
}

TTF_Font * AssetManager::GetFont(std::string id)
{
	return fonts[id];
}
