#pragma once

#ifndef AssetManager_h
#define AssetManager_h

#include <map>
#include <string>
#include "TextureManager.h"
#include "SDL.h"
#include "ECS.h"
#include "Vector2D.h"
#include "SDL_ttf.h"

class AssetManager
{
public:
	AssetManager();
	AssetManager(Manager* man);
	~AssetManager();

	// Game objects.
	void CreateProjectile(Vector2D pos, Vector2D vel, int range, int speed, std::string id);

	// Textures.
	void AddTexture(std::string id, const char* path);
	SDL_Texture* GetTexture(std::string id);

	// Fonts.
	void AddFont(std::string id, const char* path, int fontSize);
	TTF_Font* GetFont(std::string id);

private:
	Manager* manag;
	std::map<std::string, SDL_Texture*> textures;
	std::map<std::string, TTF_Font*> fonts;
};

#endif