#pragma once

#include <string>
#include <SDL.h>
#include "Components.h"
#include "TextureManager.h"

class ColliderComponent : public Component
{
public:
	SDL_Rect collider;
	std::string tag;

	TransformComponent* transform;
	SDL_Texture* tex;
	SDL_Rect srcRect, dstRect;

	ColliderComponent(std::string t)
	{
		tag = t;
	}

	ColliderComponent(std::string t, int xPos, int yPos, int size)
	{
		tag = t;
		collider.x = xPos;
		collider.y = yPos;
		collider.h = collider.w = size;
	}

	void init() override
	{
		if (!entity->hasComponents<TransformComponent>())
		{
			entity->addComponent<TransformComponent>();
		}
		transform = &entity->getComponent<TransformComponent>();

		tex = TextureManager::LoadTexture("assets/ColTex.png");
		srcRect = { 0, 0, 32, 32};
		dstRect = { collider.x, collider.y, collider.w, collider.h };

		
	}

	void update() override
	{
		if (tag != "terrain")
		{
			collider.x = static_cast<int>(transform->position.x);
			collider.y = static_cast<int>(transform->position.y);
			collider.w = static_cast<int>(transform->width * transform->scale);
			collider.h = static_cast<int>(transform->height * transform->scale);
		}

		dstRect.x = collider.x - Game::camera.x;
		dstRect.y = collider.y - Game::camera.y;
	}

	void draw() override
	{
		TextureManager::Draw(tex, srcRect, dstRect, SDL_FLIP_NONE);
	}
};