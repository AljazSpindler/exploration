#pragma once

#ifndef Game_h
#define Game_h

#include <stdio.h>
#include <iostream>
#include <string>
#include "SDL.h"
#include "SDL_image.h"
#include "TextureManager.h"
#include <vector>

class ColliderComponent;
class AssetManager;

// Game class definition. 
class Game
{
public:
	Game();
	~Game();

	void init(const char* title, int xPos, int yPos, int width, int height, bool fulscreen); // Initializes initial game parameters.
	void handleEvents(); // Handle user events - interactions.
	void update(); // Updatess all game objects in our game.
	void render(); // Display all changes to the objects - game state.
	void clean(); // Clear game objects from memory.

	bool running() { return isRunning; } // Indicates if the game is running.

	static SDL_Renderer* renderer;
	static SDL_Event event;
	static bool isRunning;
	static SDL_Rect camera;
	static AssetManager* assets;

	enum groupLabels : std::size_t
	{
		groupMap,
		groupPlayers,
		groupColliders,
		groupProjectiles
	};

private:
	int cnt = 0;
	SDL_Window* window;
};

#endif // !Game_h

