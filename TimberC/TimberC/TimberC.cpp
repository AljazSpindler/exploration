// TimberC.cpp : Defines the entry point for the console application.

#include "stdafx.h"
#include <sstream>
#include <SFML\Graphics.hpp>
#include <SFML\Audio.hpp>

using namespace sf;

// Branches;
void updateBranches(int seed);
const int NUM_BRANCHES = 6;
Sprite branches[NUM_BRANCHES];

enum class side { LEFT, RIGHT, NONE };
side branchPositions[NUM_BRANCHES];

int main()
{
	VideoMode vm(1920, 1080);

	// Window creation.
	RenderWindow window(vm, "TimberC", Style::Resize);

	// Background loading.
	Texture backgroundTex;
	backgroundTex.loadFromFile("graphics/background.png");

	Sprite backgorundSpr;
	backgorundSpr.setTexture(backgroundTex);

	backgorundSpr.setPosition(0, 0);
	//backgorundSpr.setScale(((float)800) / 1920, ((float)600) / 1080);

	// Tree
	Texture treeTex;
	treeTex.loadFromFile("graphics/tree.png");
	Sprite treeSpr;
	treeSpr.setTexture(treeTex);
	//treeSpr.setScale(0.5, 0.5);
	treeSpr.setPosition(810, 0);

	Sprite extraTrees[4];
	for (int i = 0; i < 4; i++)
	{
		extraTrees[i].setTexture(treeTex);
	}
	extraTrees[0].setPosition(210, 0);
	extraTrees[0].scale(0.5f, 1.0f);
	extraTrees[1].setPosition(600, -50);
	extraTrees[1].scale(0.3f, 1.0f);
	extraTrees[2].setPosition(1200, -200);
	extraTrees[2].scale(0.7f, 1.0f);
	extraTrees[3].setPosition(1600, -100);
	extraTrees[3].scale(0.25f, 1.0f);

	// Bee
	Texture beeTex;
	beeTex.loadFromFile("graphics/bee.png");
	Sprite beeSpr;
	beeSpr.setTexture(beeTex);
	//treeSpr.setScale(0.5, 0.5);
	beeSpr.setPosition(0, 800);

	bool beeActive = false;
	float beeSpeed = 0.0f;

	// Clouds
	Texture cloudTex;
	cloudTex.loadFromFile("graphics/cloud.png");

	Sprite cloudSprites[3];
	bool cloudActiveStates[3];
	float cloudSpeeds[3];

	float yPos = 0;
	for (int i = 0; i < 3; i++)
	{
		cloudSprites[i].setTexture(cloudTex);
		cloudSprites[i].setPosition(0.0f, yPos);

		cloudActiveStates[i] = false;
		cloudSpeeds[i] = 0.0f;
		yPos += 250;
	}

	// Framerate solving and time.
	Clock clock;

	RectangleShape timeBar;
	float timeBarWidth = 400;
	float timeBarHeight = 80;

	timeBar.setSize(Vector2f(timeBarWidth, timeBarHeight));
	timeBar.setFillColor(Color::Red);
	timeBar.setPosition((1920 / 2.0f) - timeBarWidth / 2, 980);

	Time gameTimeTotal;
	Time textUpdate;
	float timeRemaining = 6.0f;
	float timeBarWidthPerSecond = timeBarWidth / timeRemaining;

	// Paused - state handling.
	bool paused = true;

	// HUD
	int score = 0;

	Font font;
	font.loadFromFile("fonts/KOMIKAP_.ttf");

	Text msg, scoreTxt, framerate;
	msg.setFont(font);
	scoreTxt.setFont(font);
	framerate.setFont(font);

	msg.setString("Press Enter to start!");
	scoreTxt.setString("Score = 0");
	framerate.setString("FPS = 0");

	msg.setCharacterSize(75);
	scoreTxt.setCharacterSize(100);
	framerate.setCharacterSize(100);

	msg.setFillColor(Color::White);
	scoreTxt.setFillColor(Color::White);
	framerate.setFillColor(Color::White);

	FloatRect textRect = msg.getLocalBounds();
	msg.setOrigin(textRect.left + textRect.width / 2.0f, textRect.top + textRect.height / 2.0f);
	msg.setPosition(1920 / 2.0f, 1080 / 2.0f);

	scoreTxt.setPosition(20, 20);
	framerate.setPosition(800, 20);

	// Prepare branches.
	Texture branchTex;
	branchTex.loadFromFile("graphics/branch.png");

	for (int i = 0; i < NUM_BRANCHES; i++)
	{
		branches[i].setTexture(branchTex);
		branches[i].setPosition(-2000, -2000);

		branches[i].setOrigin(220, 20);
	}

	// Player
	Texture playerTex;
	playerTex.loadFromFile("graphics/player.png");

	Sprite playerSprite;
	playerSprite.setTexture(playerTex);
	playerSprite.setPosition(580, 720);

	side playerSide = side::LEFT;

	// Gravestone
	Texture graveTex;
	graveTex.loadFromFile("graphics/rip.png");

	Sprite graveSpr;
	graveSpr.setTexture(graveTex);
	graveSpr.setPosition(600, 860);

	// Axe
	Texture axeTex;
	axeTex.loadFromFile("graphics/axe.png");

	Sprite axeSpr;
	axeSpr.setTexture(axeTex);
	axeSpr.setPosition(700, 830);

	const float AXE_POSITION_LEFT = 700;
	const float AXE_POSITION_RIGHT = 1075;

	// Flying log
	Texture logTex;
	logTex.loadFromFile("graphics/log.png");

	Sprite logSpr;
	logSpr.setTexture(logTex);
	logSpr.setPosition(810, 720);

	bool logActive = false;
	float logSpeedX = 1000;
	float logSpeedY = -1500;

	bool acceptInput = false;

	// Sounds
	SoundBuffer chopBuff;
	chopBuff.loadFromFile("sound/chop.wav");
	Sound chop;
	chop.setBuffer(chopBuff);

	SoundBuffer deathBuff;
	deathBuff.loadFromFile("sound/death.wav");
	Sound death;
	death.setBuffer(deathBuff);

	SoundBuffer ootBuff;
	ootBuff.loadFromFile("sound/out_of_time.wav");
	Sound outOfTime;
	outOfTime.setBuffer(ootBuff);

	int fps = 0;

	RectangleShape rect1, rect2;

	rect1.setSize(Vector2f(650, 100));
	rect1.setFillColor(Color(100, 100, 100, 100));
	rect1.setPosition(20, 20);

	rect2.setSize(Vector2f(650, 100));
	rect2.setFillColor(Color(100, 100, 100, 100));
	rect2.setPosition(800, 20);

	while (window.isOpen())
	{
		// Handle input.
		Event ev;
		while (window.pollEvent(ev))
		{
			if (ev.type == Event::KeyReleased && !paused)
			{
				acceptInput = true;

				axeSpr.setPosition(2000, axeSpr.getPosition().y);
			}
		}

		if (Keyboard::isKeyPressed(Keyboard::Escape))
		{
			window.close();
		}

		if (Keyboard::isKeyPressed(Keyboard::Return))
		{
			paused = false;

			score = 0;
			timeRemaining = 5;

			for (int i = 1; i < NUM_BRANCHES; i++)
			{
				branchPositions[i] = side::NONE;
			}

			graveSpr.setPosition(675, 2000);

			playerSprite.setPosition(580, 720);
			acceptInput = true;

		}

		// Update scene.
		if (acceptInput)
		{
			if (Keyboard::isKeyPressed(Keyboard::Right))
			{
				playerSide = side::RIGHT;
				score++;

				timeRemaining += (2 / score) + 0.15f;

				axeSpr.setPosition(AXE_POSITION_RIGHT, axeSpr.getPosition().y);
				playerSprite.setPosition(1200, 720);

				updateBranches(score);

				logSpr.setPosition(810, 720);
				logSpeedX = -5000;
				logActive = true;

				acceptInput = false;

				chop.play();
			}

			if (Keyboard::isKeyPressed(Keyboard::Left))
			{
				playerSide = side::LEFT;
				score++;

				timeRemaining += (2 / score) + 0.15f;

				axeSpr.setPosition(AXE_POSITION_LEFT, axeSpr.getPosition().y);
				playerSprite.setPosition(580, 720);

				updateBranches(score);

				logSpr.setPosition(810, 720);
				logSpeedX = 5000;
				logActive = true;

				acceptInput = false;

				chop.play();
			}
		}

		if (!paused)
		{
			Time dt = clock.restart();
			gameTimeTotal += dt;
			textUpdate += dt;

			fps = (int)(1 / dt.asSeconds());

			timeRemaining -= dt.asSeconds();

			timeBar.setSize(Vector2f(timeBarWidthPerSecond * timeRemaining, timeBarHeight));

			if (timeRemaining <= 0.0f)
			{
				paused = true;

				msg.setString("Out of time!");

				FloatRect textRect = msg.getLocalBounds();
				msg.setOrigin(textRect.left + textRect.width / 2.0f, textRect.top + textRect.height / 2.0f);
				msg.setPosition(1920 / 2.0f, 1080 / 2.0f);

				outOfTime.play();
			}

			if (!beeActive)
			{
				beeActive = true;
				srand((int)time(0) * 10);
				beeSpeed = (rand() % 200) + 200;

				srand((int)time(0) * 10);
				float height = (rand() % 500) + 500;
				beeSpr.setPosition(2000, height);
			}
			else
			{
				beeSpr.setPosition(
					beeSpr.getPosition().x - (beeSpeed * dt.asSeconds()),
					beeSpr.getPosition().y
				);

				if (beeSpr.getPosition().x < -100)
				{
					beeActive = false;
				}
			}

			for (int i = 0; i < 3; i++)
			{
				if (!cloudActiveStates[i])
				{
					cloudActiveStates[i] = true;
					srand((int)time(0) * (10 * i + 10));
					cloudSpeeds[i] = rand() % 200;

					srand((int)time(0) * (10 * i + 10));
					float height = 0;
					if (i < 2)
					{
						height = (rand() % (150 * i + 150)) - 150 * i;
					}
					else
					{
						height = (rand() % (150 * i + 150)) - 150;
					}
					cloudSprites[i].setPosition(-200, height);
				}
				else
				{
					cloudSprites[i].setPosition(
						cloudSprites[i].getPosition().x + (cloudSpeeds[i] * dt.asSeconds()),
						cloudSprites[i].getPosition().y
					);

					if (cloudSprites[i].getPosition().x > 1920)
					{
						cloudActiveStates[i] = false;
					}
				}
			}

			if (textUpdate.asSeconds() > 0.25f)
			{
				std::stringstream ss;
				ss << "Score = " << score;
				scoreTxt.setString(ss.str());
				textUpdate = Time::Time();

				ss.str("");
				ss << "FPS = " << fps;
				framerate.setString(ss.str());

			}

			// Update branches.
			for (int i = 0; i < NUM_BRANCHES; i++)
			{
				float height = i * 150;
				if (branchPositions[i] == side::LEFT)
				{
					branches[i].setPosition(610, height);
					branches[i].setRotation(180);
				}
				else if (branchPositions[i] == side::RIGHT)
				{
					branches[i].setPosition(1330, height);
					branches[i].setRotation(0);
				}
				else
				{
					branches[i].setPosition(3000, height);
				}
			}

			if (logActive)
			{
				logSpr.setPosition(logSpr.getPosition().x + (logSpeedX * dt.asSeconds()), logSpr.getPosition().y + (logSpeedY * dt.asSeconds()));

				if (logSpr.getPosition().x < -100 || logSpr.getPosition().x > 2000)
				{
					logActive = false;
					logSpr.setPosition(810, 720);
				}
			}

			if (branchPositions[5] == playerSide)
			{
				paused = true;
				acceptInput = false;

				graveSpr.setPosition(525, 760);

				playerSprite.setPosition(2000, 660);

				msg.setString("SQUISHED!");

				FloatRect textRect = msg.getLocalBounds();

				msg.setOrigin(textRect.left + textRect.width / 2.0f, textRect.top + textRect.height / 2.0f);
				msg.setPosition(1920 / 2.0f, 1080 / 2.0f);

				death.play();
			}
		}

		// Draw scene.

		// Clear from last frame.
		window.clear();

		// Draw game objects.
		window.draw(backgorundSpr);

		for (int i = 0; i < 4; i++)
		{
			window.draw(extraTrees[i]);
		}

		for (int i = 0; i < 3; i++)
		{
			window.draw(cloudSprites[i]);
		}

		for (int i = 0; i < NUM_BRANCHES; i++)
		{
			window.draw(branches[i]);
		}

		window.draw(treeSpr);

		window.draw(playerSprite);

		window.draw(axeSpr);

		window.draw(logSpr);

		window.draw(graveSpr);

		window.draw(beeSpr);

		window.draw(rect1);
		window.draw(rect2);

		window.draw(scoreTxt);
		if (paused)
		{
			window.draw(msg);
		}

		window.draw(framerate);

		window.draw(timeBar);

		// Flips buffer.
		window.display();
	}

	return 0;
}

void updateBranches(int seed)
{
	for (int j = NUM_BRANCHES - 1; j > 0; j--)
	{
		branchPositions[j] = branchPositions[j - 1];
	}

	// Spawn new branch.
	srand((int)time(0) + seed);
	int r = (rand() % 5);
	switch (r)
	{
	case 0:
		branchPositions[0] = side::LEFT;
		break;
	case 1:
		branchPositions[0] = side::RIGHT;
		break;
	default:
		branchPositions[0] = side::NONE;
		break;
	}
}

