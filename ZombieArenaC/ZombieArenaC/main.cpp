#include "stdafx.h"
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "Player.h"
#include "ZombieArena.h"
#include "TextureHolder.h"
#include "Bullet.h"
#include "Pickup.h"
#include <iostream>
#include <sstream>
#include <fstream>

using namespace sf;
using namespace std;

int main()
{
	// Init
	TextureHolder holder; // singleton

	enum  class State
	{
		PAUSED, LEVELING_UP, GAME_OVER, PLAYING
	};

	State state = State::GAME_OVER;

	Vector2f resolution;
	resolution.x = VideoMode::getDesktopMode().width;
    resolution.y = VideoMode::getDesktopMode().height;

	float soundVol = 15.0f;

	RenderWindow window(VideoMode(resolution.x, resolution.y), "Zombia ArenaC", Style::Fullscreen);

	View mainView(sf::FloatRect(0, 0, resolution.x, resolution.y));

	// Timing.
	Clock clock;

	Time gameTimeTotal;

	Vector2f mouseWorldPosition;
	Vector2i mouseScreenPosition;

	Player player;

	IntRect arena;

	// Background
	VertexArray background;
	Texture backgroundTexture = TextureHolder::GetTexture("graphics/background_sheet.png");

	int numZombies;
	int numZombiesAlive;
	Zombie* zombies = nullptr;

	// Bulets
	Bullet bullets[100];
	int currentBullet = 0;
	int bulletsSpare = 24;
	int bulletsInClip = 6;
	int clipSize = 6;
	float fireRate = 1;
	Time lastPressed;

	// Crosshair
	window.setMouseCursorVisible(false);
	//window.setMouseCursorGrabbed(true);
	Sprite crosshairSpr;
	Texture crosshairTex = TextureHolder::GetTexture("graphics/crosshair.png");

	crosshairSpr.setTexture(crosshairTex);
	crosshairSpr.setOrigin(25, 25);

	// Pickups init
	Pickup healthPickup(1);
	Pickup ammoPickup(2);

	// About the game
	int score = 0;
	int hiScore = 0;

	// Text and HUD
	Sprite gameOverSpr;
	Texture gameOverTex = TextureHolder::GetTexture("graphics/background.png");
	gameOverSpr.setTexture(gameOverTex);
	gameOverSpr.setPosition(0, 0);

	View hudView(FloatRect(0, 0, resolution.x, resolution.y));

	Sprite ammoIconSpr;
	Texture ammoIconTex = TextureHolder::GetTexture("graphics/ammo_icon.png");
	ammoIconSpr.setTexture(ammoIconTex);
	ammoIconSpr.setPosition(28, resolution.y - 80);

	Font font;
	font.loadFromFile("fonts/zombiecontrol.ttf");

	Text pausedText;
	pausedText.setFont(font);
	pausedText.setCharacterSize(85);
	pausedText.setFillColor(Color::White);
	pausedText.setPosition(350, 200);
	pausedText.setString("Press Enter \nto continue");

	Text gameOverText;
	gameOverText.setFont(font);
	gameOverText.setCharacterSize(80);
	gameOverText.setFillColor(Color::White);
	gameOverText.setPosition(680, 540);
	gameOverText.setString("Press Enter \nto play");

	Text levelUpText;
	levelUpText.setFont(font);
	levelUpText.setCharacterSize(60);
	levelUpText.setFillColor(Color::White);
	levelUpText.setPosition(80, 150);
	stringstream levelUpStream;
	levelUpStream << "1- Increased rate of fire" <<
					 "\n2- Increased clip size(next reload)" <<
					 "\n3- Increased max health" <<
					 "\n4- Increased run speed" <<
					 "\n5- More and better health pickups" <<
					 "\n6- More and better ammo pickups)";
	levelUpText.setString(levelUpStream.str());

	Text ammoText;
	ammoText.setFont(font);
	ammoText.setCharacterSize(50);
	ammoText.setFillColor(Color::White);
	ammoText.setPosition(116, resolution.y - 80);

	Text scoreText;
	scoreText.setFont(font);
	scoreText.setCharacterSize(50);
	scoreText.setFillColor(Color::White);
	scoreText.setPosition(20, 0);

	ifstream inputFile("gamedata/scores.txt");
	if (inputFile.is_open())
	{
		inputFile >> hiScore;
		inputFile.close();
	}

	Text hiScoreText;
	hiScoreText.setFont(font);
	hiScoreText.setCharacterSize(50);
	hiScoreText.setFillColor(Color::White);
	hiScoreText.setPosition(resolution.x - 300, 0);
	stringstream s;
	s << "Hi Score:" << hiScore;
	hiScoreText.setString(s.str());

	Text zombiesRemainingText;
	zombiesRemainingText.setFont(font);
	zombiesRemainingText.setCharacterSize(50);
	zombiesRemainingText.setFillColor(Color::White);
	zombiesRemainingText.setPosition(975, resolution.y - 80);
	zombiesRemainingText.setString("Zombies: 100");

	int wave = 0;
	Text waveNumberText;
	waveNumberText.setFont(font);
	waveNumberText.setCharacterSize(50);
	waveNumberText.setFillColor(Color::White);
	waveNumberText.setPosition(750, resolution.y - 80);
	zombiesRemainingText.setString("Wave: 0");

	RectangleShape healthBar;
	healthBar.setFillColor(Color::Red);
	healthBar.setPosition(300, resolution.y - 80);

	int framesSinceLastHUDUpdate = 0;
	int fpsMeasurementFrameInterval = 1000;

	// Sounds
	SoundBuffer hitBuff;
	hitBuff.loadFromFile("sound/hit.wav");
	Sound hitSound;
	hitSound.setBuffer(hitBuff);
	hitSound.setVolume(soundVol);

	SoundBuffer splatBuff;
	splatBuff.loadFromFile("sound/splat.wav");
	Sound splatSound;
	splatSound.setBuffer(splatBuff);
	splatSound.setVolume(soundVol);

	SoundBuffer shootBuff;
	shootBuff.loadFromFile("sound/shoot.wav");
	Sound shootSound;
	shootSound.setBuffer(shootBuff);
	shootSound.setVolume(soundVol);

	SoundBuffer reloadBuff;
	reloadBuff.loadFromFile("sound/reload.wav");
	Sound reloadSound;
	reloadSound.setBuffer(reloadBuff);
	reloadSound.setVolume(soundVol);

	SoundBuffer reloadFailedBuff;
	reloadFailedBuff.loadFromFile("sound/reload_failed.wav");
	Sound reloadFailedSound;
	reloadFailedSound.setBuffer(reloadFailedBuff);
	reloadFailedSound.setVolume(soundVol);

	SoundBuffer powerupBuff;
	powerupBuff.loadFromFile("sound/powerup.wav");
	Sound powerupSound;
	powerupSound.setBuffer(powerupBuff);
	powerupSound.setVolume(soundVol);

	SoundBuffer pickupBuff;
	pickupBuff.loadFromFile("sound/pickup.wav");
	Sound pickupSound;
	pickupSound.setBuffer(pickupBuff);
	pickupSound.setVolume(soundVol);

	// Main loop.
	while (window.isOpen())
	{
		// Input
		Event event;
		while (window.pollEvent(event))
		{
			if (event.type == Event::KeyPressed)
			{
				if (event.key.code == Keyboard::Return && state == State::PLAYING)
				{
					state = State::PAUSED;
				}
				else if (event.key.code == Keyboard::Return && state == State::PAUSED)
				{
					state = State::PLAYING;
					clock.restart();
				}
				else if (event.key.code == Keyboard::Return && state == State::GAME_OVER)
				{
					state = State::LEVELING_UP;
					wave = 0;
					score = 0;

					currentBullet = 0;
					bulletsSpare = 24;
					bulletsInClip = 6;
					clipSize = 6;
					fireRate = 1;

					player.resetPlayerStats();
				}

				if (state == State::PLAYING)
				{
					if (event.key.code == Keyboard::R)
					{
						if (bulletsSpare >= clipSize)
						{
							bulletsInClip = clipSize;
							bulletsSpare -= clipSize;

							reloadSound.play();
						}
						else if (bulletsSpare > 0)
						{
							bulletsInClip = bulletsSpare;
							bulletsSpare = 0;

							reloadSound.play();
						}
						else
						{
							reloadFailedSound.play();
						}
					}
				}
			}
		}

		// WASD control
		if (Keyboard::isKeyPressed(Keyboard::Escape))
		{
			ofstream outputfile("gamedata/scores.txt");
			outputfile << hiScore;
			outputfile.close();

			window.close();
		}

		if (state == State::PLAYING)
		{
			if (Keyboard::isKeyPressed(Keyboard::W))
			{
				player.moveUp();
			}
			else
			{
				player.stopUp();
			}

			if (Keyboard::isKeyPressed(Keyboard::S))
			{
				player.moveDown();
			}
			else
			{
				player.stopDown();
			}

			if (Keyboard::isKeyPressed(Keyboard::A))
			{
				player.moveLeft();
			}
			else
			{
				player.stopLeft();
			}

			if (Keyboard::isKeyPressed(Keyboard::D))
			{
				player.moveRight();
			}
			else
			{
				player.stopRight();
			}

			if (Mouse::isButtonPressed(Mouse::Left))
			{
				if (gameTimeTotal.asMilliseconds() - lastPressed.asMilliseconds() > 1000 / fireRate && bulletsInClip > 0)
				{
					bullets[currentBullet].shoot(player.getCenter().x, player.getCenter().y, mouseWorldPosition.x, mouseWorldPosition.y);
					currentBullet++;
				    
					cout << "Player Shooting from (x,y): " << player.getCenter().x << "," << player.getCenter().x << endl;
					cout << "Player Shooting at (x,y): " << mouseWorldPosition.x << "," << mouseWorldPosition.y << endl;

					if (currentBullet > 99)
					{
						currentBullet = 0;
					}
					lastPressed = gameTimeTotal;

					shootSound.play();

					bulletsInClip--;
				}
			}
		}

		// Leveling up
		if (state == State::LEVELING_UP)
		{
			if (event.key.code == Keyboard::Num1)
			{
				fireRate++;
				state = State::PLAYING;
			}

			if (event.key.code == Keyboard::Num2)
			{
				clipSize += clipSize;
				state = State::PLAYING;
			}

			if (event.key.code == Keyboard::Num3)
			{
				player.upgradeHealth();
				state = State::PLAYING;
			}

			if (event.key.code == Keyboard::Num4)
			{
				player.upgradeSpeed();
				state = State::PLAYING;
			}

			if (event.key.code == Keyboard::Num5)
			{
				healthPickup.upgrade();
				state = State::PLAYING;
			}

			if (event.key.code == Keyboard::Num6)
			{
				ammoPickup.upgrade();
				state = State::PLAYING;
			}

			if (state == State::PLAYING)
			{
				// Wave increase
				wave++;

				// Level init.
				arena.width = 500 * wave;
				arena.height = 500 * wave;
				arena.left = 0;
				arena.top = 0;

				int tileSize = createBackground(background, arena);

				player.spawn(arena, resolution, tileSize);

				healthPickup.setArena(arena);
				ammoPickup.setArena(arena);

				numZombies = 5 * wave;
				delete[] zombies;
				zombies = createHorde(numZombies, arena);
				numZombiesAlive = numZombies;

				powerupSound.play();

				clock.restart();
			}
		}

		// Frame update
		if (state == State::PLAYING)
		{
			Time dt = clock.restart();
			gameTimeTotal += dt;
			float dtAsSeconds = dt.asSeconds();

			mouseScreenPosition = Mouse::getPosition();
			mouseWorldPosition = window.mapPixelToCoords(Mouse::getPosition(), mainView);

			crosshairSpr.setPosition(mouseWorldPosition);

			player.update(dtAsSeconds, Mouse::getPosition());

			Vector2f playerPositon(player.getCenter());
			mainView.setCenter(playerPositon);

			for (int i = 0; i < numZombies; i++)
			{
				if (zombies[i].isAlive())
				{
					zombies[i].update(dtAsSeconds, playerPositon);
				}
			}

			for (int i = 0; i < 100; i++)
			{
				if (bullets[i].isInFlight())
				{
					bullets[i].update(dtAsSeconds);
				}
			}

			healthPickup.update(dtAsSeconds);
			ammoPickup.update(dtAsSeconds);

			for (int i = 0; i < 100; i++)
			{
				for (int j = 0; j < numZombies; j++)
				{
					if (bullets[i].isInFlight() && zombies[j].isAlive())
					{
						if (bullets[i].getPosition().intersects(zombies[j].getPosition()))
						{
							bullets[i].stop();

							if (zombies[j].hit())
							{
								score += 10;
								if (score >= hiScore)
								{
									hiScore = score;
								}

								numZombiesAlive--;

								if (numZombiesAlive == 0)
								{
									state = State::LEVELING_UP;
								}
							}
							splatSound.play();
						}
					}
				}
			}

			for (int i = 0; i < numZombies; i++)
			{
				if (player.getPosition().intersects(zombies[i].getPosition()) && zombies[i].isAlive())
				{
					if (player.hit(gameTimeTotal))
					{
						hitSound.play();
					}

					if (player.getHealth() <= 0)
					{
						state = State::GAME_OVER;
						ofstream outputfile("gamedata/scores.txt");
						outputfile << hiScore;
						outputfile.close();
					}
				}
			}

			if (player.getPosition().intersects(healthPickup.getPosition()) && healthPickup.isSpawned())
			{
				player.increaseHealthLevel(healthPickup.gotIt());

				pickupSound.play();
			}

			if (player.getPosition().intersects(ammoPickup.getPosition()) && ammoPickup.isSpawned())
			{
				bulletsSpare += ammoPickup.gotIt();

				reloadSound.play();
			}

			healthBar.setSize(Vector2f(player.getHealth() * 3, 70));
			framesSinceLastHUDUpdate++;

			if (framesSinceLastHUDUpdate > fpsMeasurementFrameInterval)
			{
				stringstream ssAmmo;
				stringstream ssScore;
				stringstream ssHiScore;
				stringstream ssWave;
				stringstream ssZombiesAlive;

				ssAmmo << bulletsInClip << "/" << bulletsSpare;
				ammoText.setString(ssAmmo.str());

				ssScore << "Score:" << score;
				scoreText.setString(ssScore.str());
				
				ssHiScore << "Hi Score:" << hiScore;
				hiScoreText.setString(ssHiScore.str());

				ssWave << "Wave:" << wave;
				waveNumberText.setString(ssWave.str());

				ssZombiesAlive << "Zombies:" << numZombiesAlive;
				zombiesRemainingText.setString(ssZombiesAlive.str());

				framesSinceLastHUDUpdate = 0;
			}
		}

		// Render
		if (state == State::PLAYING)
		{
			window.clear();

			window.setView(mainView);

			window.draw(background, &backgroundTexture);

			for (int i = 0; i < numZombies; i++)
			{
				window.draw(zombies[i].getSprite());
			}

			for (int i = 0; i < 100; i++)
			{
				if (bullets[i].isInFlight())
				{
					window.draw(bullets[i].getShape());
				}
			}

			window.draw(player.getSprite());

			if (ammoPickup.isSpawned())
			{
				window.draw(ammoPickup.getSprite());
			}
			if (healthPickup.isSpawned())
			{
				window.draw(healthPickup.getSprite());
			}

			window.draw(crosshairSpr);

			window.setView(hudView);

			window.draw(ammoIconSpr);
			window.draw(ammoText);
			window.draw(scoreText);
			window.draw(hiScoreText);
			window.draw(healthBar);
			window.draw(waveNumberText);
			window.draw(zombiesRemainingText);
		}

		if (state == State::LEVELING_UP)
		{
			window.draw(gameOverSpr);
			window.draw(levelUpText);
		}

		if (state == State::PAUSED)
		{
			window.draw(pausedText);
		}

		if (state == State::GAME_OVER)
		{
			window.draw(gameOverSpr);
			window.draw(gameOverText);
			window.draw(scoreText);
			window.draw(hiScoreText);
		}

		window.display();
	}

	delete[] zombies;

	return 0;
}