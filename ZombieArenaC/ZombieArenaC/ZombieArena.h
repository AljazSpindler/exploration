#pragma once
#include "Zombie.h"

using namespace sf;
int createBackground(VertexArray& eVA, IntRect arena);
Zombie* createHorde(int numZombies, IntRect arena);

