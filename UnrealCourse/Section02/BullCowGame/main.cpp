/* MAIN ENTRY POINT of the program. Game Bulls and Cows 2018.
*/

#pragma once

#include <iostream>
#include <string>
#include "FBullCowGame.h"

// To make syntax Unreal friendly.
using FText = std::string;
using int32 = int;

// Protos.
void PrintIntroduction();
void PlayGame();
void PrintGameSummary();
FText GetValidGuess();
bool AskToPlayAgain();

FBullCowGame BCGame; // The main game class.

// Application Entry point.
int main()
{
	do
	{
		PrintIntroduction();
		PlayGame();
	}
	while(AskToPlayAgain());

	return 0;
}

// Introduce the game.
void PrintIntroduction()
{
	std::cout << std::endl << std::endl;
	std::cout << "---------------------------------------------------------" << std::endl;
	std::cout << "| * * *    * * *       * * *                            |" << std::endl;
	std::cout << "| *    *  *     *     *     *                           |" << std::endl;
	std::cout << "| * * *    * * *     *                                  |" << std::endl;
	std::cout << "| *    *   * * *     *                                  |" << std::endl;
	std::cout << "| *    *  *   * *     *     *                           |" << std::endl;
	std::cout << "| * * *    * *    *    * * *                            |" << std::endl;
	std::cout << "---------------------------------------------------------" << std::endl;
	std::cout << "Welcome to Bulls and Cows, a fun word game." << std::endl;

	/*int32 WordLen = 5;
	FString CLen = "";
	do
	{
		std::cout << std::endl << "Select the word length (3 - 7 characters): ";
		std::getline(std::cin, CLen);
		std::stringstream parse(CLen);

		if (WordLen < 3 || WordLen > 7)
		{
			std::cout << std::endl << "Invalid word length! Pick a number between 3 and 7!" << std::endl;
		}
	} while (WordLen < 3 || WordLen > 7);

	BCGame.SetHiddenWordByLength(WordLen);*/

	std::cout << "So can you guess the " << BCGame.GetHiddenWordLength() << " letter isogram I'm thinking of?" << std::endl << std::endl;

	return;
}

// Plays a single game to completion.
void PlayGame()
{
	int32 MaxTries = BCGame.GetMaxTries();

	// Get our guesses.
	while(!BCGame.IsGameWon() && BCGame.GetCurrentTry() < BCGame.GetMaxTries())
	{
		FText Guess = GetValidGuess();
		
		// Submit valid guess.
		FBullCowCount BullCowCount = BCGame.SubmitValidGuess(Guess);

		std::cout << "Number of bulls: " << BullCowCount.Bulls << ", number of cows: " << BullCowCount.Cows << std::endl;
		std::cout << "You have " << (BCGame.GetMaxTries() - BCGame.GetCurrentTry()) << " guesses left!" << std::endl << std::endl;
	}

	PrintGameSummary();
	BCGame.Reset();
	
	return;
}

void PrintGameSummary()
{
	if (BCGame.IsGameWon())
	{
		std::cout << "You won the game!" << std::endl;
	}
	else
	{
		std::cout << "You lost! Better luck next time!" << std::endl;
	}
}

// Get a guess.
FText GetValidGuess()
{
	EGuessStatus Status = EGuessStatus::Invalid_Status;
	FText Guess = "";

	do
	{
		std::cout << "Try " << BCGame.GetCurrentTry() + 1 << ". Enter your guess: ";
		std::getline(std::cin, Guess);

		Status = BCGame.CheckGuessValidity(Guess);

		switch (Status)
		{
		case EGuessStatus::Not_Isogram:
			std::cout << "The word is not an isogram!" << std::endl << std::endl;
			break;
		case EGuessStatus::Invalid_Length:
			std::cout << "Please enter a " << BCGame.GetHiddenWordLength() << " letter word!" << std::endl << std::endl;
			break;
		case EGuessStatus::Not_Lowercase:
			std::cout << "The word entered is not in lower case!" << std::endl << std::endl;
			break;
		case EGuessStatus::Multiple_Words:
			std::cout << "You have entered multiple words!" << std::endl << std::endl;
			break;
		case EGuessStatus::Empty:
			break;
		default:
			break;
		}
	} while (Status != EGuessStatus::OK);

	return Guess;
}

bool AskToPlayAgain()
{
	std::cout << "Do you want to play again with the same word(y/n)? ";
	FText Response = "";
	std::getline(std::cin, Response);

	return (Response[0] == 'y' || Response[0] == 'Y');
}
