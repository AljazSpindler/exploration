#pragma once
#include "FBullCowGame.h"
#include <map>

// To make syntax Unreal friendly.
#define TMap std::map
using int32 = int;

FBullCowGame::FBullCowGame()
{
	Reset();
}

int32 FBullCowGame::GetMaxTries() const
{
	TMap<int32,int32> WordLengthToMaxTries = { {3, 3}, {4, 3}, {5, 4}, {6, 5}, {7, 6} };
	return WordLengthToMaxTries[HiddenWord.length()];
}

int32 FBullCowGame::GetCurrentTry() const
{
	return MyCurrenTry;
}

int32 FBullCowGame::GetHiddenWordLength() const
{
	return HiddenWord.length();
}

bool FBullCowGame::IsGameWon() const
{
	return bGameWon;
}

void FBullCowGame::Reset()
{
	const FString HIDDEN_WORD = "planet"; // This must be an isogram.
	
	MyCurrenTry = 0;
	HiddenWord = HIDDEN_WORD;
	bGameWon = false;
	
	return;
}

EGuessStatus FBullCowGame::CheckGuessValidity(FString Guess) const
{
	if (!IsIsogram(Guess))
	{
		return EGuessStatus::Not_Isogram;
	}
	else if (!IsLowercase(Guess))
	{
		return EGuessStatus::Not_Lowercase;
	}
	else if (GetHiddenWordLength() != Guess.length())
	{
		return EGuessStatus::Invalid_Length;
	}
	else
	{
		return EGuessStatus::OK;
	}
}

void FBullCowGame::SetHiddenWordByLength(int32 WordLength)
{
	std::map<int32, FString> WordLengthToHiddenWord = { { 3, "Blu" },{ 4, "Kaka" },{ 5, "Pluki" },{ 6, "Planet" },{ 7, "Planets" } };
	HiddenWord = WordLengthToHiddenWord[WordLength];
}

bool FBullCowGame::IsIsogram(FString Word) const
{
	// Treat 0 and 1 letter words as isograms.
	if (Word.length() <= 1)
	{
		return true;
	}

	std::map<char, bool> LetterSeen;
	for (auto Letter : Word)
	{
		Letter = tolower(Letter);
		if (LetterSeen[Letter])
		{
			return false;
		}
		else
		{
			LetterSeen[Letter] = true;
		}
	}

	return true;
}

bool FBullCowGame::IsLowercase(FString Word) const
{
	if (Word.length() <= 1)
	{
		return true;
	}

	for (auto Letter : Word)
	{
		if (!islower(Letter))
		{
			return false;
		}
	}

	return true;
}

FBullCowCount FBullCowGame::SubmitValidGuess(FString Guess)
{
	MyCurrenTry++;
	FBullCowCount BullCowCount;

	int32 WordLength = HiddenWord.length();
	for (int32 HWChar = 0; HWChar < WordLength; HWChar++)
	{
		for (int32 GChar = 0; GChar < WordLength; GChar++)
		{
			if (HiddenWord[HWChar] == Guess[GChar] && HWChar == GChar)
			{
				BullCowCount.Bulls++;
			}
			else if (HiddenWord[HWChar] == Guess[GChar] && HWChar != GChar)
			{
				BullCowCount.Cows++;
			}
		}
	}

	if (BullCowCount.Bulls == WordLength)
	{
		bGameWon = true;
	}
	else
	{
		bGameWon = false;
	}

	return BullCowCount;
}
