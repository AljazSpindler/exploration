/* The game logic class. */

#pragma once
#include <string>

// To make syntax Unreal friendly.
using FString = std::string;
using int32 = int;

struct FBullCowCount
{
	int32 Bulls = 0;
	int32 Cows = 0;
};

enum class EGuessStatus
{
	Invalid_Status,
	OK,
	Not_Isogram,
	Invalid_Length,
	Not_Lowercase,
	Multiple_Words,
	Empty
};

/*
enum class EResetStatus
{
	No_Hidden_Word,
	OK
};
*/

class FBullCowGame
{
public:
	FBullCowGame();

	int32 GetMaxTries() const;
	int32 GetCurrentTry() const;
	int32 GetHiddenWordLength() const;
	bool IsGameWon() const;
	EGuessStatus CheckGuessValidity(FString Word) const;

	void SetHiddenWordByLength(int32 WordLength);

	void Reset();
	FBullCowCount SubmitValidGuess(FString Guess);

private:
	int32 MyCurrenTry;
	FString HiddenWord;
	bool bGameWon;

	bool IsIsogram(FString Word) const;
	bool IsLowercase(FString Word) const;
};