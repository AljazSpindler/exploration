#include "stdafx.h"
#include "TextureHolder.h"
#include <assert.h>

TextureHolder* TextureHolder::m_s_Instance = nullptr;

TextureHolder::TextureHolder()
{
	assert(m_s_Instance == nullptr);
	m_s_Instance = this;
}

Texture & TextureHolder::GetTexture(string const & filename)
{
	auto& m = m_s_Instance->m_Textures;
	auto kvp = m.find(filename);

	if (kvp != m.end())
	{
		return kvp->second;
	}
	else
	{
		auto& texture = m[filename];
		texture.loadFromFile(filename);

		return texture;
	}
}
