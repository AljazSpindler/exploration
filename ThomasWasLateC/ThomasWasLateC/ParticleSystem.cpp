#include "stdafx.h"
#include <SFML/Graphics.hpp>
#include "ParticleSystem.h"

using namespace sf;
using namespace std;

void ParticleSystem::draw(RenderTarget & target, RenderStates states) const
{
	target.draw(m_Vertices, states);
}

void ParticleSystem::init(int count)
{
	m_Vertices.setPrimitiveType(Points);
	m_Vertices.resize(count);

	for (int i = 0; i < count; i++)
	{
		srand(time(0) + i);
		float angle = (rand() % 360) * 3.14f / 180.0f;
		float speed = (rand() % 600) + 600.0f;

		Vector2f direction;

		direction = Vector2f(cos(angle) * speed, sin(angle) * speed);

		m_Particles.push_back(Particle(direction));
	}
}

void ParticleSystem::emitParticles(Vector2f position)
{
	m_IsRunning = true;
	m_Duration = 2;

	vector<Particle>::iterator i;
	int currentVertex = 0;

	for (i = m_Particles.begin(); i != m_Particles.end(); i++)
	{
		m_Vertices[currentVertex].color = Color::Yellow;
		(*i).setPosition(position);

		currentVertex++;
	}
}

void ParticleSystem::update(float elapsed)
{
	m_Duration -= elapsed;
	vector<Particle>::iterator i;
	int currentVertex = 0;

	for (i = m_Particles.begin(); i != m_Particles.end(); i++)
	{
		(*i).update(elapsed);

		m_Vertices[currentVertex].position = (*i).getPosition();

		currentVertex++;
	}

	if (m_Duration < 0)
	{
		m_IsRunning = false;
	}
}

bool ParticleSystem::running()
{
	return m_IsRunning;
}
